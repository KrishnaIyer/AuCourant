AuCourant
=========
This project is an Android News Reader written in Java.


Disclaimer
==========
This project/App relies on third-party APIs for the actual data.
This app takes no responsibility for the credibility of the data.

News API
--------
* Query news from around the world. Please check the link below:
    * https://newsapi.org/docs/get-started

* An API KEY is required for it. As a developer, it's free.

* This example contains a key that I made. Please use your own keys for your projects. Especially since it'f free.



CoinMarketCap API
-----------------
This is a special API for the https://coinmarketcap.com/, an absolutely amazing website that provides financial info on all of the major blockchains.


JSON Parsing
------------
Android has native JSON Parsing
* https://stackoverflow.com/questions/9605913/how-to-parse-json-in-android


Disclaimer
==========
Please Note that this project is for academic purposes only. If you plan to use this for commercial purposes, make sure to
* Adhere to the GPL3 License.
* Purchase the required Licenses for the APIs used.

This project contains developer/free licenses for some APIs and they may not be used for commercial purposes.
I will not be held responsible if you misuse this example App.


License
=========
Copyright (c) 2017 Krishna Iyer Easwaran

This project is released under the Apache license. Please refer to the LICENSE file.

