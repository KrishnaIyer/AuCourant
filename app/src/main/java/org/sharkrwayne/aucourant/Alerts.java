package org.sharkrwayne.aucourant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.sharkrwayne.logic.NewsAPIWrapper;


/**
 * Main Activity
 *
 * @author  Krishna Iyer (mail@krishnaiyereaswaran.com)
 * @since  20 Nov 2017
 *
 */

public class Alerts extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerts);


        Button sButton = (Button) findViewById(R.id.searchButton);

        sButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NewsAPIWrapper newsAPIWrapper = new NewsAPIWrapper();

                newsAPIWrapper.get("BlockChain");

                Toast toast = Toast.makeText(getApplicationContext(),"Goodbye Cruel World",Toast.LENGTH_SHORT);
                toast.show();

            }
        });


    }
}
